# ruby

Install and manage Ruby

## Dependencies

* [polka.asdf](https://gitlab.com/discr33t/polka/asdf.git)
  _The asdf configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `versions`:
    * Type: List
    * Usages: List of Ruby versions to install

* `global_version`:
    * Type: String
    * Usages: Ruby version to make global default

* `gemrc_config`:
    * Type: List
    * Usages: gemrc file settings

```
terraform:
  versions:
    - 0.11.8
    - 0.11.10
  global_version: 0.11.10
  gem_config:
    - install: --no-document --no-ri --no-rdoc
    - update: --no-document --no-ri --no-rdoc
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polka.ruby

## License

MIT
